import React from "react";
import VidwSocket from './components/view/ViewSocket';
import Users from './components/view/Users';

function App() {
  return (
    <div>
      <VidwSocket />
      <Users />
    </div>
  );
}

export default App;
