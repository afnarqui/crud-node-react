import axios from "axios";

class UsersService {
    
    constructor() {
    }

    addUsers(nombre, apellido, email, telefono) {
        const baseURL = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_PORT}/api/`;
        return axios.post(`${baseURL}users?nombre=${nombre}&apellido=${apellido}&email=${email}&telefono=${telefono}`, {nombre})
            .then((response) => {
                return response.data;
            })
            .catch((err) => {
                return 'err' + err
            });
    }

    
    updateUsers(id, nombre, apellido, email, telefono) {
        const baseURL = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_PORT}/api/`;
        return axios.put(`${baseURL}users?id=${id}&nombre=${nombre}&apellido=${apellido}&email=${email}&telefono=${telefono}`, {nombre})
            .then((response) => {
                return response.data;
            })
            .catch((err) => {
                return 'err' + err
            });
    }

    deleteUsers(id) {
        const baseURL = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_PORT}/api/`;
        return axios.delete(`${baseURL}users?id=${id}`, {})
            .then((response) => {
                return response.data;
            })
            .catch((err) => {
                return 'err' + err
            });
    }    

    allUsers() {
        const baseURL = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_PORT}/api/`;
        return axios.get(`${baseURL}users`)
            .then((response) => {
                return response.data;
            })
            .catch((err) => {
                return 'err' + err
            });
    }    
    
}

export default UsersService;
