import React, { useState, useEffect } from "react";
import socketIOClient from "socket.io-client";

import Table from 'react-bootstrap/Table'

const ENDPOINT = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_PORT}`;

const ViewSocket = (props) => {
  const [response, setResponse] = useState([]);
  const [values, setValues] = useState([]);

  useEffect(() => {
      const socket = socketIOClient(ENDPOINT);
      socket.on("messages", items => {
          if(items.length>0){
           setResponse(items);
           if(response.length>0) {
            let cantidadReal = response.length;
            let count = response[response.length - 1].count;
            var responseOrigin = response.splice(cantidadReal - count);
            let data = []
            for(var i = 0; i < count;i++) {
              data.push({
                nombre: responseOrigin[i].nombre,
                value1: responseOrigin[i].value1,
                value2: responseOrigin[i].value2,
                value3: responseOrigin[i].value3,
              })
            }
            setValues(data)
            }
          }
       });
       return () => socket.disconnect();
 })
    return (
        <Table  style={{ display: "block", textAlign: 'center'}} striped bordered hover size="sm">
                  {
           values.length > 0 ? 
     (
        values.map((e,i) =>
         <thead key={i}>
         <tr>
           <th>Nombre Value1 Value2 Value3</th>
         
         </tr>
        
         <tbody>
          <tr>
            <td>{e.nombre}</td>
        
           <td>{e.value1}</td>
         
              <td>{e.value2}</td>
          
            <td>{e.value3}</td>
            </tr>
          </tbody>
          </thead>
     
         ))
          :
         null
       } 
      </Table>
    );
};

export default ViewSocket;
