import React from "react";
import UsersService from "../../../api/UsersService";
import Table from 'react-bootstrap/Table'

const Users = (props) => {
    const [nombre, setNombre] = React.useState("");
    const [apellido, setApellido] = React.useState("");
    const [email, setEmail] = React.useState("");
    const [telefono, setTelefono] = React.useState("");
    const [id, setId] = React.useState("");
    const [updated, setUpdated] = React.useState(false);
    const [items, setItems] = React.useState([]);
  
    const handleSubmit = async (e) => {
      e.preventDefault();
      if(nombre !== undefined && apellido !== undefined && email !== undefined && telefono !== undefined){
          let data = {
              nombre,
              apellido,
              email,
              telefono
          }
       add(data);
      } else {
        alert('items required');
      }
    }

    const deleteData = async (id) => {
      const usersService = new UsersService();
      const data = await usersService.deleteUsers(id);
      alert(`items deleted with success`);
      const valuesNew = await usersService.allUsers();
      setItems(valuesNew);
      cleanValues();
    }

    const add = async (values) => {
        const usersService = new UsersService();
        if(updated) {
          const data = await usersService.updateUsers(
            id,
            values.nombre,
            values.apellido,
            values.email,
            values.telefono
        );
         alert(`items update success`);
        } else {
          const data = await usersService.addUsers(
            values.nombre,
            values.apellido,
            values.email,
            values.telefono
        );
        alert(`items add success`);
        }
        
        const valuesNew = await usersService.allUsers();
        setItems(valuesNew);
        cleanValues();
    }

    const updateData = (id) => {
      let data = items.filter((item) => item.id == id)
      setNombre(data[0].nombre)
      setApellido(data[0].apellido);
      setEmail(data[0].email)
      setTelefono(data[0].telefono)
      setId(data[0].id);
      setUpdated(true);
    }

    const cleanValues = () => {
      setNombre("")
      setApellido("");
      setEmail("")
      setTelefono("")
      setId("");
      setUpdated(false);
    }

    return (
      <div>
        <form style={{ display: "block", textAlign: 'center'}} onSubmit={handleSubmit}>
        <div>
        <label htmlFor="nombre">Nombre</label>
        <input
          type="text"
          id="nombre"
          value={nombre}
          onChange={(e) => setNombre(e.target.value)}
        />
        </div>
        <div>
        <label htmlFor="apellido">Apellido</label>
        <input
          type="text"
          id="apellido"
          value={apellido}
          onChange={(e) => setApellido(e.target.value)}
        />
        </div>
        <div>
        <label htmlFor="email">Email</label>
        <input
          type="email"
          id="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        </div>
        <div>
        <label htmlFor="telefono">Teléfono</label>
        <input
          type="tel"
          id="telefono"
          value={telefono}
          onChange={(e) => setTelefono(e.target.value)}
        />
        </div>
        <button type="submit">Guardar</button>
      </form>

      <Table  style={{ display: "block", textAlign: 'center'}} striped bordered hover size="sm">
                  {
           items.length > 0 ? 
     (
      items.map((e,i) =>
         <thead key={i}>
         <tr>
           <th>Nombre Apellido Email Teléfono</th>
         </tr>
        
         <tbody>
          <tr>
            <td>{e.nombre}</td>
        
           <td>{e.apellido}</td>
         
              <td>{e.email}</td>
          
            <td>{e.telefono}</td>
            <td>{e.id}</td>
            <td><button style={{ background: "green", color: "white"}} type="button" onClick={() => updateData(e.id)}>Update</button></td>
            <td><button style={{ background: "red", color: "white"}}  type="button" onClick={() => deleteData(e.id)}>Delete</button></td>
            </tr>
          </tbody>
          </thead>
     
         ))
          :
         null
       } 
      </Table>
      
      </div>
    );
};

export default Users;
