# crud-node-react

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/andresnaranjo-marcapersonal.appspot.com/o/prueba-sabado%2Fprueba.JPG?alt=media&token=a1e3d34e-abdd-4bdc-81d0-4b1cf4790a9e)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

project crud-node-react. developed in nodejs using react

[video](https://firebasestorage.googleapis.com/v0/b/andresnaranjo-marcapersonal.appspot.com/o/prueba-sabado%2Fprueba%20node%20react.mp4?alt=media&token=23158207-27e2-4254-97e3-f35119fc0460) Vídeo test

[trello](https://trello.com/b/UwSBWWHd/nodereact) Trello

And of course crud-node-react itself is open source with a [public repository][afn]
on GitLab.

## Usage

crud-node-react requires

[Git](https://git-scm.com/downloads)

[Node](https://nodejs.org/es/)

to run.

Install the dependencies and devDependencies previous
start the server.

## download

```sh
git clone https://gitlab.com/afnarqui/crud-node-react
cd crud-node-react
cd back
npm i
npm run dev
cd ..
cd front
npm i 
npm run start
```

## run aplication in developement

```sh
http://localhost:3000/
```

[afn]: https://gitlab.com/afnarqui/crud-node-react