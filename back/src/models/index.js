require('dotenv').config();
const config = require('../../config');
const { v4: uuidv4 } = require('uuid');

let users = []
const query = {};

/**
 * Gets the user
 */
query.get = (request, res, params) => {
  return res.status(200).send(users);
};

/**
 * Makes a users
 */
query.post = (request, res, params) => {
  let { nombre, apellido, email, telefono } = params;
  let uuid = uuidv4();
  users.push({
    id:uuid, nombre,apellido,email,telefono
  })
  console.log(params)
  res.status(200).send(params);
};

/**
 * Updates an existing users
 */
query.put = (request, res, params) => {
  let { id, nombre, apellido, email, telefono } = params;
  const data = users.filter( item => item.id !== id);
  data.push({
    id, nombre,apellido,email,telefono
  })
  users = [];
  users = data;
  res.status(201).send({
    id, nombre,apellido,email,telefono
  });
};

/**
 * Deletes an existing users
 */
query.delete = (request, res, params) => {
  let { id } = params;
  const data = users.filter( item => item.id !== id);
  users = [];
  users = data;
  res.status(201).send({
    'response': `el id ${id} item deleted`
  });
};

module.exports = query;
