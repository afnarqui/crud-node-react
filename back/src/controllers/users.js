const user = require('../models');

const query = {};

/**
 * Gets the user 
 */
query.get = (req, res) => {
  user.get(req, res, req.query);
};

/**
 * Makes a user
 */
query.post = (req, res) => {
  user.post(req, res, req.query);
};

/**
 * Updates an existing user
 */
query.put = (req, res) => {
  user.put(req, res, req.query);
};

/**
 * Deletes an existing user
 */
query.delete = (req, res) => {
  user.delete(req, res, req.query);
};

module.exports = query;
