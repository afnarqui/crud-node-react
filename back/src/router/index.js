const express = require('express');
const user = require('../controllers/users');

const router = express.Router();

/**
 * Gets the users
 */
router.get('/api/users', function (req, res) {
  user.get(req, res, req.body);
});

/**
 * Makes a user
 */
router.post('/api/users', function (req, res) {
  user.post(req, res, req.body);
});

/**
 * Updates an existing user
 */
router.put('/api/users', function (req, res) {
  user.put(req, res, req.body);
});

/**
 * Deletes an existing user
 */
router.delete('/api/users', function (req, res) {
  user.delete(req, res, req.body);
});

module.exports = router;
