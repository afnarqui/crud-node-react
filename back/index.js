require('dotenv').config();
const bodyParser = require('body-parser');
const fs = require('fs');
const cors = require('cors');
const express = require("express");
const http = require("http");
const socketIo = require("socket.io");
const app = express();
const server = http.createServer(app);
const io = socketIo(server);
const routes = require('./src/router');

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(express.static(__dirname + '/public'));

app.use('/', routes);

app.use(
  cors({
    origin: true,
    credentials: true,
  })
);

let dataFiles = []
let nombre
let value1
let value2
let value3
let count
fs.watch(__dirname +'/valores.txt', (event, filename) => {
	fs.readFile(filename, 'utf8', (err, contents) => {
     const values = contents.split('\r\n');
     if(values != '') {
        count = values.length
        for(var i =0; i< values.length; i++) {
          let valoresNu = values[i] !== null && values[i] !== undefined ? values[i] : ',';
          let nuevosValores = valoresNu.split(',')
          for(var k = 0; k < nuevosValores.length; k++){
            if( k === 0) {
             nombre = nuevosValores[k]
            }
            if( k === 1) {
             value1 = nuevosValores[k]
            }
            if( k === 2) {
             value2 = nuevosValores[k]
            }
            if( k === 3) {
             value3 = nuevosValores[k]
           }
         }
        dataFiles.push({ nombre,
          value1,
          value2,
          value3,
          count})
    }
  }
  })
})

app.all('/*', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', 'localhost:3000');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header(
    'Access-Control-Allow-Headers',
    'Content-type,Accept,X-Access-Token,X-Key'
  );
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});

io.on('connection', ( socket ) => {
  setInterval(() => {
    socket.emit('messages', valoresgenerales())  
  }, 1000);
})

const valoresgenerales = () => {
  return dataFiles;
}
server.listen(process.env.PORT, () => {
  console.log('aplication running in the port: ', process.env.PORT);
});
